import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTables1640789679085 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(
      `CREATE TABLE product (
        id INT AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        description TEXT NOT NULL,
        PRIMARY KEY (id)
        );`,
    );

    queryRunner.query(
      `CREATE TABLE user (
          id INT AUTO_INCREMENT,
          email VARCHAR(50) NOT NULL UNIQUE,
          password VARCHAR(200) NOT NULL,
          PRIMARY KEY (id)
          );`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE product;`);
    await queryRunner.query(`DROP TABLE user;`);
  }
}
