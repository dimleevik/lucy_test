FROM node:14-slim

WORKDIR /app

COPY migration ./migration
COPY src ./src
COPY *.json ./

RUN npm ci \
    && npm run build \
    && npm cache clean --force

EXPOSE 3000
