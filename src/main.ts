import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { swagger } from './common/swagger/swagger';
import { AppModule } from './core/AppModule';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  swagger(app);
  await app.listen(3000);
}
bootstrap();
