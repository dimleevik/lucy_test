import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ProductService } from '../services/ProductService';
import { DbProductDto } from '../types/DbProductDto';
import { PaginatedResponse } from '../types/PaginatedResponse';
import { ProductDto } from '../types/ProductDto';
import { ProductIdParam } from '../types/ProductIdParam';
import { ProductQueryParams } from '../types/ProductQueryParams';
import { UpdateProductDto } from '../types/UpdateProductDto';

@ApiTags('Product methods')
@ApiExtraModels(PaginatedResponse)
@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @ApiOkResponse({
    description: 'Record found',
    type: PaginatedResponse,
  })
  @Get('/')
  list(@Query() queryParam: ProductQueryParams) {
    return this.productService.list(queryParam);
  }

  @ApiOkResponse({
    description: 'Record found',
    type: DbProductDto,
  })
  @Get('/:id')
  index(@Param() { id }: ProductIdParam) {
    return this.productService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Created',
    type: DbProductDto,
  })
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  async create(@Body() productData: ProductDto) {
    return await this.productService.create(productData);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/:id')
  update(
    @Param() { id }: ProductIdParam,
    @Body() productData: UpdateProductDto,
  ) {
    return this.productService.update(id, productData);
  }
}
