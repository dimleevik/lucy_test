import { CacheModule, Module } from '@nestjs/common';
import { DbModule } from 'src/common/storage/DbModule';
import { AuthModule } from 'src/core/auth/AuthModule';
import { ProductController } from './controllers/ProductController';
import { ProductService } from './services/ProductService';

@Module({
  imports: [DbModule, AuthModule, CacheModule.register()],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}
