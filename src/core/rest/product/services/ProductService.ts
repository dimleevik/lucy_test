import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { ProductEntity } from 'src/common/storage/entities/product/ProductEntity';
import { DbService } from 'src/common/storage/services/DbService';
import { ProductDto } from '../types/ProductDto';
import { ProductQueryParams } from '../types/ProductQueryParams';
import { UpdateProductDto } from '../types/UpdateProductDto';

@Injectable()
export class ProductService {
  constructor(
    private dbService: DbService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async list(params: ProductQueryParams) {
    const limit = params.limit || 10;
    const offset = (params.offset || 0) * (params.limit || 10);
    const [items, count] = await this.dbService.findAll(ProductEntity, {
      take: limit,
      skip: offset,
    });
    return { items, count };
  }

  async findOne(id: number) {
    let data = await this.cacheManager.get(`product-${id}`);
    if (!data) {
      data = await this.dbService.findOne(ProductEntity, { where: { id } });
      await this.cacheManager.set(`product-${id}`, data, { ttl: 60 });
    }
    return data;
  }

  async create(productData: ProductDto) {
    return await this.dbService.create<ProductDto>(ProductEntity, productData);
  }

  async update(id: number, productData: UpdateProductDto) {
    return await this.dbService.update(ProductEntity, { id }, productData);
  }
}
