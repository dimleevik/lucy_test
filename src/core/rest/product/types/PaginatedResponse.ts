import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { DbProductDto } from './DbProductDto';

export class PaginatedResponse {
  @ApiProperty({ type: [DbProductDto] })
  items: DbProductDto[];

  @ApiProperty()
  count: number;
}
