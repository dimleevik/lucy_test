import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNumberString } from 'class-validator';

export class ProductIdParam {
  @ApiProperty()
  @IsNumberString()
  id: number;
}
