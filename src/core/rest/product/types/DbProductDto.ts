import { ApiProperty } from '@nestjs/swagger';
import { ProductDto } from './ProductDto';

export class DbProductDto extends ProductDto {
  @ApiProperty()
  id: number;
}
