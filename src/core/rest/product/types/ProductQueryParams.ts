import { ApiProperty } from '@nestjs/swagger';
import { IsNumberString } from 'class-validator';

export class ProductQueryParams {
  @ApiProperty({ required: false })
  limit?: number;

  @ApiProperty({ required: false })
  offset?: number;
}
