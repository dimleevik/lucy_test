import { Module } from '@nestjs/common';
import { DbModule } from 'src/common/storage/DbModule';
import { AuthModule } from 'src/core/auth/AuthModule';
import { AuthController } from './controllers/UserController';
import { UserService } from './services/UserService';

@Module({
  imports: [DbModule, AuthModule],
  controllers: [AuthController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
