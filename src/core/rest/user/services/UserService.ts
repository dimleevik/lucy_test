import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UserEntity } from 'src/common/storage/entities/user/UserEntity';
import { DbService } from 'src/common/storage/services/DbService';
import { AuthService } from 'src/core/auth/services/AuthService';
import { CreateUserDto } from '../types/CreateUserDto';

@Injectable()
export class UserService {
  constructor(private dbService: DbService, private authService: AuthService) {}

  async login(userData: CreateUserDto) {
    const user = await this.authService.validateUser(userData);
    if (!user) {
      throw new UnauthorizedException();
    }
    return this.authService.login(user);
  }

  async register(createUser: CreateUserDto) {
    const isExist = await this.dbService.findOne<CreateUserDto>(UserEntity, {
      where: { email: createUser.email },
    });
    if (isExist) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'User already exist',
        },
        HttpStatus.CONFLICT,
      );
    }
    const user = await this.dbService.create<CreateUserDto>(
      UserEntity,
      createUser,
    );

    return { ...user, password: '' };
  }
}
