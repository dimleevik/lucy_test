import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UserService } from '../services/UserService';
import { CreateUserDto } from '../types/CreateUserDto';
import { DbUserDto } from '../types/DbUserDto';
import { UserToken } from '../types/UserToken';

@ApiTags('Auth methods')
@Controller('auth')
export class AuthController {
  constructor(private userService: UserService) {}

  @ApiOkResponse({
    description: 'Authorized',
    type: UserToken,
  })
  @ApiUnauthorizedResponse({})
  @Post('login')
  auth(@Body() createUser: CreateUserDto) {
    return this.userService.login(createUser);
  }

  @ApiOkResponse({
    description: 'Registered',
    type: DbUserDto,
  })
  @Post('register')
  register(@Body() createUser: CreateUserDto) {
    return this.userService.register(createUser);
  }
}
