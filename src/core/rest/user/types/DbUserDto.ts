import { ApiProperty } from '@nestjs/swagger';
import { CreateUserDto } from './CreateUserDto';

export class DbUserDto extends CreateUserDto {
  @ApiProperty()
  id: number;
}
