import { Module } from '@nestjs/common';
import { DbModule } from 'src/common/storage/DbModule';
import { ProductModule } from './product/ProductModule';
import { UserModule } from './user/UserModule';

@Module({
  imports: [ProductModule, UserModule],
})
export class RestModule {}
