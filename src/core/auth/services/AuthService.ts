import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UserEntity } from 'src/common/storage/entities/user/UserEntity';
import { DbService } from 'src/common/storage/services/DbService';
import { CreateUserDto } from 'src/core/rest/user/types/CreateUserDto';

@Injectable()
export class AuthService {
  constructor(private dbService: DbService, private jwtService: JwtService) {}

  public async validateUser(userData: CreateUserDto): Promise<any> {
    const user = await this.dbService.findOne<CreateUserDto>(UserEntity, {
      where: { email: userData.email },
    });
    let checkPassword = false;
    if (user) {
      checkPassword = await this.comparePassword(
        userData.password,
        user.password,
      );
    }
    return checkPassword ? { ...user, password: '' } : null;
  }

  async login(user: any) {
    const payload = { username: user.email, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  private async comparePassword(password, hash) {
    return bcrypt.compare(password, hash);
  }
}
