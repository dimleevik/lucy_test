import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DbModule } from 'src/common/storage/DbModule';
import { AuthModule } from './auth/AuthModule';
import { RestModule } from './rest/RestModule';

@Module({
  imports: [RestModule, DbModule, AuthModule],
})
export class AppModule {}
