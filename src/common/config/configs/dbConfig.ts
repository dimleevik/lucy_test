import { registerAs } from '@nestjs/config';
import { initDbConfig } from './initDbConfig';

export default registerAs('dbConfig', () => initDbConfig());
