import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  public getDbConfig() {
    return this.configService.get<TypeOrmModuleOptions>('dbConfig');
  }
}
