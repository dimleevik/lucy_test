import * as fs from 'fs';
import * as dotenv from 'dotenv';
import { initDbConfig } from '../configs/initDbConfig';

const filePath = process.env.NODE_ENV ? `.${process.env.NODE_ENV}.env` : '.env';
const envData = {};
dotenv.config({ path: filePath });
Object.assign(envData, initDbConfig(), {
  migrationsTableName: 'migration_table',
  migrations: ['migration/*.ts'],
  cli: {
    migrationsDir: 'migration',
  },
});

export default envData;
