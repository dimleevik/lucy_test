import { Injectable } from '@nestjs/common';
import {
  Connection,
  EntityTarget,
  FindManyOptions,
  FindOneOptions,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

@Injectable()
export class DbService {
  constructor(private connection: Connection) {}

  async create<T>(entityClass: EntityTarget<any>, value: T) {
    const entity = this.connection.manager.create<T>(entityClass, value);
    const repo = this.connection.manager.getRepository(entityClass);
    return repo.save(entity);
  }

  findOne<T>(entityClass: EntityTarget<any>, findOptions: FindOneOptions = {}) {
    return this.connection.manager.findOne<T>(entityClass, findOptions);
  }

  findAll<T>(
    entityClass: EntityTarget<any>,
    findOptions: FindManyOptions = {},
  ) {
    return this.connection.manager.findAndCount(entityClass, findOptions);
  }

  update<T>(
    entityClass: EntityTarget<any>,
    criteria: any,
    updateData: QueryDeepPartialEntity<T>,
  ) {
    return this.connection.manager.update(entityClass, criteria, updateData);
  }
}
