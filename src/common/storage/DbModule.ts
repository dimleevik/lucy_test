import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { ProductEntity } from './entities/product/ProductEntity';
import { UserEntity } from './entities/user/UserEntity';
import { DbService } from './services/DbService';
import { AppConfigModule } from '../config/AppConfigModule';
import { AppConfigService } from '../config/services/AppConfigService';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [AppConfigModule],
      inject: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => ({
        ...appConfigService.getDbConfig(),
        entities: [UserEntity, ProductEntity],
      }),
    }),
  ],
  providers: [DbService],
  exports: [DbService],
})
export class DbModule {}
