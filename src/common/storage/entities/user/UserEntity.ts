import * as bcrypt from 'bcrypt';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { length: 50, unique: true })
  email: string;

  @Column('varchar', { length: 80 })
  password: string;

  @BeforeUpdate()
  @BeforeInsert()
  async genHash() {
    this.password = await bcrypt.hash(this.password, 6);
  }
}
