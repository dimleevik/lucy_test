Требования:

- Docker version 19.03.15
- docker-compose version 1.29.2

Запустить проект можно с помощью команды:
`docker-compose -f docker-compose up -d`

Документация swagger: `http://127.0.0.1:3000/api`
